'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'app/main/main.html'
        });
    }]);

})(angular);
