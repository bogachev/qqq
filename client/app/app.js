'use strict';

(function (angular) {
  angular.module('SubscribeVacancyApp', [
    'ngResource',
    'ngSanitize',
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'textAngular',
    'toaster'
  ])
    .config(function ($urlRouterProvider, $locationProvider) {
      $urlRouterProvider.otherwise('/');
      $locationProvider.html5Mode(true);
    });

})(angular);
