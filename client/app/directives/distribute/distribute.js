'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .directive('distributeForm', function () {
      return {
        restrict: "AE",
        controller: 'DistributeCtrl',
        controllerAs: 'vm',
        scope: {},
        templateUrl: 'app/directives/distribute/distribute.html'
      };
    });

})(angular);
