'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .config(configTextEditor)
    .controller('DistributeCtrl', DistributeCtrl);

  configTextEditor.$inject = ['$provide'];
  function configTextEditor($provide) {
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function (taRegisterTool, taOptions) {
      taOptions.toolbar = [
        ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
      ];
      return taOptions;
    }]);
  }

  DistributeCtrl.$inject = ['modalComponent', 'CategoriesService', 'CitiesService', 'SalaryService', 'ApiService'];
  function DistributeCtrl(modalComponent, CategoriesService, CitiesService, SalaryService, ApiService) {
    var vm = this;

    vm.data = {
      categoryList: CategoriesService.getAll(),
      cityList: CitiesService.getAll(),
      salaryList: SalaryService.getAll()
    };

    vm.model = {
      category: "",
      city: "",
      salaryRange: "",
      email: ""
    };

    vm.toggleCategorySelected = toggleCategorySelected;
    vm.toggleCitySelected = toggleCitySelected;
    vm.toggleSalarySelected = toggleSalarySelected;
    vm.formSubmit = formSubmit;

    function toggleCategorySelected(value) {
      vm.model.category = value;
    }

    function toggleCitySelected(value) {
      vm.model.city = value;
    }

    function toggleSalarySelected(value) {
      vm.model.salaryRange = value;
    }

    function formSubmit() {
      var modal;

      var formCallback = function (data) {

        var formData = _.merge(
          vm.model,
          _.omit(data, ['dismissable'])
        );

        ApiService
          .newVacancy(formData)
          .then(function () {
            var message = modalComponent.show({
              title: 'Success',
              html: '<p>New vacancy was created!</p>',
              buttons: [
                {
                  text: 'Ok',
                  classes: ['btn-success'],
                  click: function () {
                    vm.model = {};
                    message.close();
                  }
                }
              ]
            });
          });
      };

      modal = modalComponent.show({
        salaryRange: vm.model.salaryRange
      }, {
        template_url: 'app/directives/distribute/modal.template.html',
        size: 'lg',
        controller: DistributeVacancyModalCtrl
      });

      modal.result.then(function (data) {
        if (data)
          formCallback(data);
      });
    }

  }

  DistributeVacancyModalCtrl.$inject = ['$uibModalInstance', 'modal'];
  function DistributeVacancyModalCtrl($uibModalInstance, modal) {
    var vm = this;
    vm.model = modal;

    vm.dismiss = function () {
      $uibModalInstance.dismiss();
    };

    vm.saveChanges = function () {
      $uibModalInstance.close(vm.model);
    }
  }

})(angular);
