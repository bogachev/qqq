'use strict';

(function () {
  angular.module('SubscribeVacancyApp')
    .controller('SubscriptionCtrl', SubscriptionCtrl);

  SubscriptionCtrl.$inject = ['CategoriesService', 'CitiesService', 'SalaryService', 'ApiService', 'modalComponent'];
  function SubscriptionCtrl(CategoriesService, CitiesService, SalaryService, ApiService, modalComponent) {

    var vm = this;

    vm.data = {
      categoryList: CategoriesService.getAll(),
      cityList: CitiesService.getAll(),
      salaryList: SalaryService.getAll(),
      selectedCategories: "",
      selectedCities: "",
      selectedSalary: ""
    };

    vm.model = {
      categories: [],
      city: null,
      salaryRange: null,
      email: null
    };

    vm.toggleCategorySelected = toggleCategorySelected;
    vm.toggleCitySelected = toggleCitySelected;
    vm.toggleSalarySelected = toggleSalarySelected;
    vm.formSubmit = formSubmit;

    function toggleCategorySelected(category) {
      var index = vm.model.categories.indexOf(category);
      if (index >= 0) {
        vm.model.categories.splice(index, 1);
      } else {
        if (vm.model.categories.length < 3) {
          vm.model.categories.push(category);
        }
      }
      vm.data.selectedCategories = vm.model.categories ? vm.model.categories.join(', ') : "";
    }

    function toggleCitySelected(value) {
      vm.model.city = value;
      vm.data.selectedCities = value;
    }

    function toggleSalarySelected(value) {
      vm.model.salaryRange = value;
      vm.data.selectedSalary = value;
    }

    function formSubmit() {
      var modal;

      var mbOk = function () {
        vm.model = {};
        vm.data.selectedCategories = "";
        vm.data.selectedCities = "";
        vm.data.selectedSalary = "";
        modal.close();
      };

      ApiService.newSubscription(vm.model)
        .then(function () {
          modal = modalComponent.show({
            title: 'Success',
            html: '<p>New subscription was created!</p>',
            buttons: [
              {
                text: 'Ok',
                classes: ['btn-success'],
                click: mbOk
              }
            ]
          });
        });
    }

  }

})(angular);
