'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .directive('subscriptionForm', function () {
      return {
        restrict: "AE",
        controller: 'SubscriptionCtrl',
        controllerAs: 'vm',
        scope: {},
        templateUrl: 'app/directives/subscription/subscription.html'
      };
    });

})(angular);
