'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .config(function ($stateProvider) {
      $stateProvider
        .state('admin', {
          url: '/admin',
          templateUrl: 'app/admin/admin.html',
          controller: 'AdminCtrl',
          controllerAs: 'vm'
        });
    });

})(angular);
