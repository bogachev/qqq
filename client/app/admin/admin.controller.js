'use strict';

(function (angular) {
  angular.module('SubscribeVacancyApp')
    .controller('AdminCtrl', AdminCtrl);

  AdminCtrl.$inject = ['$http', 'toaster', 'modalComponent', 'ApiService'];
  function AdminCtrl($http, toaster, modalComponent, ApiService) {

    var vm = this;

    vm.vacancyList = [];
    vm.subscriptionsList = [];

    vm.removeVacancy = removeVacancy;
    vm.removeSubscription = removeSubscription;
    vm.sendSubscription = sendSubscription;

    initController();

    function initController() {
      ApiService.getVacancies()
        .then(function (response) {
          vm.vacancyList = response.data;
        });

      ApiService.getSubscriptions()
        .then(function (response) {
          vm.subscriptionsList = response.data;
        });
    }

    function removeVacancy(item) {
      var modal;

      var mbYes = function () {
        ApiService.removeVacancy(item._id)
          .then(function () {
            modal.close();
            vm.vacancyList = _.filter(vm.vacancyList, function (x) {
              return x._id != item._id;
            });
          });
      };

      var closeModal = function () {
        modal.close();
      };

      modal = modalComponent.show({
        title: 'Confirm Delete',
        html: '<p>Are you sure you want to delete the vacancy?</p>',
        buttons: [
          {
            text: 'Yes',
            classes: ['btn-success'],
            click: mbYes
          },
          {
            text: 'No',
            classes: ['btn-default'],
            click: closeModal
          }
        ]
      });
    }

    function removeSubscription(item) {
      var modal;

      var mbYes = function () {
        ApiService.removeSubscription(item._id)
          .then(function () {
            modal.close();
            vm.subscriptionsList = _.filter(vm.subscriptionsList, function (x) {
              return x._id != item._id;
            });
          });
      };

      var closeModal = function () {
        modal.close();
      };

      modal = modalComponent.show({
        title: 'Confirm Delete',
        html: '<p>Are you sure you want to delete the subscription?</p>',
        buttons: [
          {
            text: 'Yes',
            classes: ['btn-success'],
            click: mbYes
          },
          {
            text: 'No',
            classes: ['btn-default'],
            click: closeModal
          }
        ]
      });
    }

    function sendSubscription(item) {
      ApiService.sendSubscription(item._id)
        .then(function () {
          toaster.pop('success', "Notification sent");
        }, function () {
          toaster.pop('error', "Error");
        });
    }

  }

})(angular);
