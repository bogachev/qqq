'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp').service('SalaryService', SalaryService);

  function SalaryService() {
    var salaries = [
      {title: '1000-2000', from: 1000, to: 2000},
      {title: '2000 - 5000', from: 2000, to: 5000},
      {title: '5000-10000', from: 5000, to: 10000}
    ];

    this.getAll = function () {
      return salaries;
    }
  }

})(angular);
