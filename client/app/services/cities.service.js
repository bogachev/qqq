'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp').service('CitiesService', CitiesService);

  function CitiesService() {
    var cities = ['New York', 'Berlin', 'Paris'];

    this.getAll = function () {
      return cities;
    }
  }

})(angular);
