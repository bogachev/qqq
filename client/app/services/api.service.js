'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .service('ApiService', ApiService);

  ApiService.$inject = ['$http'];
  function ApiService($http) {

    return {
      newVacancy: newVacancy,
      getVacancies: getVacancies,
      removeVacancy: removeVacancy,
      newSubscription: newSubscription,
      getSubscriptions: getSubscriptions,
      removeSubscription: removeSubscription,
      sendSubscription: sendSubscription
    };

    function request(method, url, data) {
      var parameters = {
        method: method,
        url: url
      };

      if (data) {
        parameters.data = data;
      }

      return $http(parameters);
    }

    // Vacancies

    function newVacancy(data) {
      return request('POST', '/api/vacancy', data);
    }

    function getVacancies() {
      return request('GET', '/api/vacancies');
    }

    function removeVacancy(vacancy_id) {
      return request('DELETE', '/api/vacancy/' + vacancy_id);
    }

    // Subscriptions

    function newSubscription(data) {
      return request('POST', '/api/subscription', data);
    }

    function getSubscriptions() {
      return request('GET', '/api/subscriptions');
    }

    function removeSubscription(subscription_id) {
      return request('DELETE', '/api/subscription/' + subscription_id);
    }

    function sendSubscription(subscription_id) {
      return request('POST', '/api/subscription/' + subscription_id + '/send');
    }

  }

})(angular);
