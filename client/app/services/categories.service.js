'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .service('CategoriesService', CategoriesService);

  function CategoriesService() {
    var categories = ['Medicine', 'IT', 'Political', 'Teaching'];

    this.getAll = function () {
      return categories;
    }
  }

})(angular);
