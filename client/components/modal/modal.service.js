'use strict';

(function (angular) {

  angular.module('SubscribeVacancyApp')
    .factory('modalComponent', modalComponent);

  ModalCtrl.$inject = ['modal'];
  function ModalCtrl(modal) {
    var vm = this;
    vm.modal = modal;
  }

  modalComponent.$inject = ['$uibModal'];
  function modalComponent($uibModal) {

    return {
      show: showModal
    };

    function showModal(parameters, options) {

      return $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: window._.get(options, 'template_url', 'components/modal/modal.html'),
        size: _.get(options, 'size', false),
        controller: _.get(options, 'controller', ModalCtrl),
        controllerAs: 'vm',
        backdrop: 'static',
        windowClass: 'custom-modal',
        backdropClass: 'custom-backdrop',
        resolve: {
          modal: function () {
            return _.assign({
              dismissable: true
            }, parameters)
          }
        }
      });
    }
  }

})(angular);
