'use strict';

var express = require('express');
var router = express.Router();
var subscriptionController = require('./subscription.controller');
var vacancyController = require('./vacancy.controller');

router.get('/subscriptions', subscriptionController.list);
router.post('/subscription', subscriptionController.create);
router.delete('/subscription/:id', subscriptionController.remove);
router.post('/subscription/:id/send', subscriptionController.send)

router.get('/vacancies', vacancyController.list);
router.post('/vacancy', vacancyController.create);
router.delete('/vacancy/:id', vacancyController.remove);

module.exports = router;


