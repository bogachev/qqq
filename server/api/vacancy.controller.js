'use strict';

import Vacancy from '../models/vacancy.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

export function list(req, res) {
  Vacancy.findAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function create(req, res) {
  Vacancy.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function remove(req, res) {
  Vacancy.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
