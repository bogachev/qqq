'use strict';

import config from '../config/environment';
import _ from "lodash";

var domain = 'sandbox7cb9cee46b5a4187bd47d785878ce6ae.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: config['mailgun']['apiKey'], domain: domain});

import Subscription from '../models/subscription.model';
import Vacancy from '../models/vacancy.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    res.status(statusCode).send(err);
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

export function list(req, res) {
  Subscription.findAsync()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function create(req, res) {
  Subscription.createAsync(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function remove(req, res) {
  Subscription.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function send(req, res) {
  Subscription.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(function (entity) {
      var query = {city: entity.city, category: {$in: entity.categories}};

      return Vacancy.findAsync(query).then((data) => {
        let promises = [];
        _.each(data, (vacancy) => {
          console.log(vacancy);
          var data = {
            from: '#COMPANY# <#EMAIL#>'
              .replace('#COMPANY#', vacancy.company)
              .replace('#EMAIL#', vacancy.email),
            to: entity.email,
            subject: "New vacancy",
            html: vacancy.description
          };

          let pr = new Promise((resolve, reject) => {
            mailgun.messages().send(data, function (error) {
              if (error) {
                console.log(error);
                reject();
              }
              resolve();
            });
          });
          promises.push(pr);
        });

        return Promise.all(promises).then(() => {
          return {send: true};
        }).catch(() => {
          return {send: false}
        });
      });
    })
    .then(respondWithResult(res, 200))
    .catch(handleError(res));
}
